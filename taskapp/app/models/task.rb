class Task < ActiveRecord::Base
  belongs_to :project

  validates :title, presence: true

  scope :unifinished, -> {where(done: false)}
end
